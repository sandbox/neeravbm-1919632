<?php

/**
 * Returns heartbeat activity.
 */
function _heartbeat_resource_retrieve($uid, $class) {
	if ($heartbeatStream = heartbeat_stream($class, 0, user_load($uid))) {
		// Changes at runtime.
		$heartbeatStream->setOffsetTime(0);
		$heartbeatStream->setIsPage(FALSE);

		heartbeat_stream_build($heartbeatStream);
		if ($heartbeatStream->hasErrors()) {
			services_error(implode(" ", $heartbeatStream->getErrors()), 406);
		}

		return $heartbeatStream->messages;
	}
	else {
		return services_error(t('You are not allowed to see this activity stream.'), 401);
	}
}
